<?php

class Message
{
	public $message = "Welcome to my application";

	public function getData()
	{
		echo $this->message."<br>";
	} 
	public function setData($text)
	{
		 $this->message = $text;
	}
}
$ab = new Message;
$ab->getData();
$ab->setData('Message Created Successfully');
$ab->getData();
$ab->setData('Message Edited Successfully');
$ab->getData();
$ab->setData('Message Deleted Successfully');
$ab->getData();
$ab->setData('You are Successfully logged in');
$ab->getData();