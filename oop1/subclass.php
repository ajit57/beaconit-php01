<?php

class Student
{
	//property & method
	public $name = "Amir"; //property
	public $class = 9;
	public function info() //method
	{
		echo " Can Read<br>";
	}
	public function details()
	{
		echo $this->name. " read in class ". $this->class;
	}
}
class MathStudent extends Student
{
	public $name = "Rahim";
	public function info()
	{
		//parent::info();
		$a = new Student();
		$a->info();
		echo " Can write";
	}
	public function university()
	{
		echo " is from University of Chittagong.";
	}
}

$a = new MathStudent;
echo $a->name;
$a->info();
$a->university();