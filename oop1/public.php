<?php 
////public 

class Human 
{
	protected $name = "Jamal";
	protected function info()
	{
		echo " lives in Chittagong";
	}

	public function getInfo()
	{
		echo $this->name;
		$this->info();
	}
}

/*$obj = new Human;
$obj->getInfo();
$obj->info();*/

class Teacher extends Human
{
	public function getInfoTeacher()
	{
		//parent::info();

		$obj = new Human;
		$obj->info();

	}
}

$obj = new Teacher;
$obj->getInfoTeacher();
