<?php

class Human 
{
	protected $name = "Jamal";
	private $mobile = "SAMSUNG";

	protected function info()
	{
		echo " lives in Chittagong";
	}

	private function canRead()
	{
		echo " Can read sms";
	}

	public function getPrivate()
	{
		echo $this->mobile;
		$this->canRead();
	}

}

class Teacher extends Human
{
	public function getPrivateOfParent()
	{
		parent::canRead();
	}
}

$a = new Teacher;
//echo $a->mobile;
$a->getPrivateOfParent();