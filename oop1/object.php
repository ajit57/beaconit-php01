<?php

class Student
{
	//property & method
	public $name = "Amir"; //property
	public $class = 9;
	public function info() //method
	{
		echo " Can Read<br>";
	}
	public function details()
	{
		echo $this->name. " read in class ". $this->class;
	}
}

$obj = new Student; //making a object
echo $obj->name; 
$obj->info();
$obj->details();