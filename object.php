<?php

class Robot
{
	public $name = "Sophia"; //property
	public $age = "35"; //property
	public function talk() //method
	{
		echo "can Talk...";
	}
	public function getAge()
	{
		//accessing property in a method
		echo "I am ".$this->age."years old";
	}
}

$a = new Robot; //making a object
echo $a->name."<br>"; //accessing property by object
$a->talk()."<br>";//accessing method by object
$a->getAge()."<br>";

define('BIT', "Beacon IT");
echo BIT;